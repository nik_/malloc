//
// Created by Nikita Melnikov on 31/12/2018.
//

#include <sys/types.h>

/*
 * Structure for the block header (32 bytes) that contains pointers to
 * next (next) and previous (previous) blocks, whenever the block is
 * free or not (free) and the size of the the blocks (size) which includes
 * the header itself (the 32 bytes).
 *
 * Used typedef to make referencing of the block header easier in future.
 */

typedef struct block_header *block_h;
struct block_header {
        size_t size;
        int free;
        block_h next;
        block_h prev;
};

/*
 * Main functions definitions of _malloc and _free.
 *
 * _malloc takes an input of defining the required size of memory
 * needed by the program and gets said memory by requesting it from OS
 * or finding free block to assign. Returns pointer.
 *
 * _free takes an input of a pointer to the block that as allocated by
 * _malloc, then sets it to free.
 */

block_h ptr_to_header(void *ptr);
void * locate_block(int mode, size_t size);
void * extend_heap(size_t size);

void * _malloc(size_t size);
void _free(void * ptr);