# malloc

Git Repository for a custom implementation of malloc using the C language. This
implementation is based on theory learnt from lectures, put into practice during
coursework, and improved in this version.

### What is malloc?
Malloc is a function part of the standard C libraries which allows for dynamic
allocation of memory to be made by the developer, allowing for controlled use
of the heap. This helps to allow for the program to be more dynamic rather than
static, changing / requesting extra memory from the OS when needed.

Example of use (taken from Wikipedia):
```c
// Allocate space for an array with ten elements of type int.
int *ptr = (int *) malloc(10 * sizeof (int));
if (ptr == NULL) {
    /* Memory could not be allocated, the program should
     * handle the error here as appropriate.
     */
} else {
    // Allocation succeeded.  Do something.
    free(ptr);  /* We are done with the int objects, and
                 * free the associated pointer.
                 */
    ptr = NULL; /* The pointed-to-data  must not be used again,
                 * unless re-assigned by using malloc again.
                 */
}
```

### Executing / Using Code from this GitLab
This project repository contains unit tests which were used to check that the code
functioning as it should. To use the custom malloc and free implementations simply
use `#include "memory_management.h"`, then reference the functions using `_malloc()`
and `_free()` syntax. Take note of the `_` used as to differentiate the function call
from the actual malloc and free implementations.

### To-do List
- [x] Create .h file
- [x] Create .c file
- [x] Unit test support (minUnit)
    - [x] Unit tests for _malloc
    - [ ] Unit tests for _free
- [x] Implement _malloc
    - [x] End of block list finder function
    - [x] Block that fits needed size finder function
    - [x] Block split function
- [ ] Implement _free
    - [x] Joining of adjacent free blocks
    - [ ] Freeing free full blocks of memory at the end of list
- [ ] Implement padding