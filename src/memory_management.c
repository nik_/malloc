//
// Created by Nikita Melnikov on 31/12/2018.
//
#include <sys/types.h>
#include "../include/memory_management.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>

/*
 * Quick definition of the header size for quick referencing.
 */

#define HEADER_SIZE sizeof(struct block_header)

/*
 * Initialising the head of the linked list, which simply points to
 * the first block in the heap (if there is one), otherwise points
 * to NULL.
 *
 * Initially set to NULL as no blocks made yet. Note, its 8 bytes.
 */

void *head = NULL;

/*
 * Function to find the actual address of a block, given that the
 * returned address from malloc is +32 of that actual start of the
 * block header.
 *
 * INPUT:
 *      ptr     - Pointer to the block
 *
 * OUTPUT:
 *      block   - Pointer to start of block header
 */

block_h ptr_to_header(void *ptr) {
    return (block_h)ptr - 1;
}

/*
 * Function to locate either the end of the linked list (mode = 0) or
 * locate a block with available space that fits the size (mode = 1).
 *
 * INPUT:
 *      mode    - The requested mode, 0 for last block, 1 for select size
 *      size    - Requested size for mode 1
 *
 * OUTPUT:
 *      block   - Void pointer to the located block.
 */

void * locate_block(int mode, size_t size) {
    if (head == NULL) return NULL;
    block_h block = head;
    // Code loops through the linked list until the next pointer is NULL
    while (block != NULL) {
        if (mode) {
            if (block->size >= (size + (size_t) HEADER_SIZE) && block->free) return block;
        }
        else if (block->next == NULL) break;
        block = block->next;
    }
    if (mode) return NULL;
    else return (void *) block;
}

/*
 * Function to extend the heap, outputs a pointer to the newly created
 * block.
 *
 * INPUT:
 *      size        - Size needed
 *
 * OUTPUT:
 *      new_block   - Void pointer to the new block header
 */

void * extend_heap(size_t size) {
    block_h prev_block = (block_h) locate_block(0, 0);
    // Finding what multiple of 4096 is needed given the size input
    size_t requested_size = ceil((size + HEADER_SIZE) / 4096.0) * 4096;
    block_h new_block = sbrk(requested_size);
    if (new_block == (void *) -1) {
        // sbrk failed (reached end of memory or else)
        return NULL;
    }
    new_block->size = requested_size;
    new_block->free = 1;
    new_block->next = NULL;
    new_block->prev = prev_block;
    // If this is the first block then seat the head to point to it
    if (prev_block == head) head = new_block;
    else prev_block->next = new_block;

    return new_block;
}

/*
 * _malloc implementation, look at "memory_management.h" for  basic
 * explanation.
 *
 * INPUT:
 *      size    - The size needed in bytes.
 *
 * OUTPUT:
 *      block   - Void pointer to the start of the block head.
 */

void * _malloc(size_t size) {
    if (size == 0)
        return NULL;
    // If block of appropriate size exists then allocate the block
    block_h block = (block_h) locate_block(1, size + (size_t) HEADER_SIZE);
    if (block == NULL) {
        block = extend_heap(size);
        if (block == NULL) return NULL;
    }
    // If the block size is bigger than the requested size including header
    // then split the free block up into the taken block and free block
    if (block->size > size + (size_t) HEADER_SIZE) {
        block->free = 0;
        block_h split_block = (void *) block + size + (size_t) HEADER_SIZE;
        split_block->free = 1;
        split_block->size = block->size - (size + (size_t) HEADER_SIZE);
        split_block->next = block->next;
        // If the original block wasn't last
        if (block->next != NULL) block->next->prev = split_block;
        split_block->prev = block;
        block->size = size + (size_t) HEADER_SIZE;
        block->next = split_block;
    }
    else if (block->size == size + (size_t) HEADER_SIZE) {
        block->free = 0;
    }
    // Adding 32 bits to account for header
    return (block+1);
}

/*
 * _free implementation, look at "memory_management.h" for explanation.
 *
 * INPUT:
 *      ptr     - Pointer to block you want to free.
 */

void _free(void *ptr) {
    if (ptr == 0 || ptr == NULL) return;
    block_h block = ptr_to_header(ptr);
    if (block->free) return;
    block->free = 1;
    // Merging previous block if free and not null (linked list way)
    if (block->prev != NULL && block->prev->free) {
        block_h previous_block = block->prev;
        previous_block->size += block->size;
        previous_block->next = block->next;
        block->next->prev = previous_block;
        block = previous_block;
    }
    // Doing the same with next block
    if (block->next != NULL && block->next->free) {
        block_h next_block = block->next;
        block->size += next_block->size;
        block->next = next_block->next;
        if (next_block->next != NULL) next_block->next->prev = block;
    }
}