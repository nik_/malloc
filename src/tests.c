/*
 * Unit Testing for the custom malloc function.
 *
 * Code format copied from the same location as the MinUnit source code.
 */
#include "../include/minunit.h"
#include <stdio.h>
#include <stdlib.h>
#include "../include/memory_management.h"

int tests_run = 0;

static char * test_locating_no_blocks() {
    printf("Running test_locating_no_blocks()...\n");
    mu_assert("[FAIL] locate_block(1, 200) != NULL", (block_h) locate_block(1, 200) == NULL);
    return 0;
}

static char * test_locating_last_block_null() {
    printf("Running test_locating_last_block_null()...\n");
    mu_assert("[FAIL] locate_block(0, 0) != NULL", (block_h) locate_block(0, 0) == NULL);
    return 0;
}

static char * test_malloc_no_size() {
    printf("Running test_malloc_no_size()...\n");
    mu_assert("[FAIL] _malloc(0) != NULL", _malloc(0) == NULL);
    return 0;
}

static char * test_malloc_200() {
    printf("Running test_malloc_200()...\n");
    block_h block = ptr_to_header(_malloc(200));
    mu_assert("[FAIL] _malloc(200)->size != 232", block->size == (size_t) 232);
    mu_assert("[FAIL] _malloc(200)->next != (void *) _malloc(200) + 232", block->next == (void *) block + (size_t) 232);
    mu_assert("[FAIL] locate_block(0, 0) != block->next", (block_h) locate_block(0, 0) == block->next);
    mu_assert("[FAIL] locate_block(1, 200) != block->next", (block_h) locate_block(1, 200) == block->next);
    return 0;
}

static char * test_free() {
    printf("Running test_free()...\n");
    void * block_1 = _malloc(200);
    void * block_2 = _malloc(200);
    // Here we test if splitting function works
    _free(block_1);
    _free(block_2);
    block_h test = (block_h) locate_block(0, 0);
    mu_assert("[FAIL] locate_block(0, 0)->prev->prev != NULL", test->prev->prev == NULL);
    return 0;
}

// Run tests
static char * all_tests() {
    mu_run_test(test_locating_no_blocks);
    mu_run_test(test_locating_last_block_null);
    mu_run_test(test_malloc_no_size);
    mu_run_test(test_malloc_200);
    mu_run_test(test_free);
    return 0;
}

int main(int argc, char **argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else {
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);

    return result != 0;
}